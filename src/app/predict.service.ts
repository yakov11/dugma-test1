import { HttpClient } from '@angular/common/http';
import {  Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url ="https://pm3syupnh3.execute-api.us-east-1.amazonaws.com/beta";
  text;
  
  classifyRes;

  classify(text:string, income:string){
    this.classifyRes = [text];
    let json=
      {'text':text,
        'income':income
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        this.classifyRes.push(res);
       
      })
    )
    
    
  }
  giveBack(){
    return this.classifyRes;
  }

  constructor(private http:HttpClient, public router:Router) { }
  
}
