import { PredictService } from './../predict.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  text;
  category;
  

  classify(){
    this.predictService.classify(this.text, this.text).subscribe(
      res => {

      }
    )
    this.router.navigate(['/customers']);
  }
  constructor(private predictService:PredictService, private router:Router) { }

  ngOnInit(): void {
  }

}
