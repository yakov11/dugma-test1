import { Component,OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';
import { PredictService } from '../predict.service';

@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers:Customer[];
  customers$;
  userId:string;
  addCustomerFormOpen= false;

  years:number;
  income:number;
  ans = this.predictService.giveBack();
  categories:object = {0:'Business', 1:'Entertainment', 2:'Politics', 3:'Sport', 4:'Tech', 5:'Life', 6:'Energy'};
  classify ;
  text;
  
  foods = ['Business', 'Entertainment', 'Politics', 'Sport', 'Tech', 'Life', 'Energy']
  
id:string;

  prediction='unknown'; 
  
  show(){
    this.ans = this.predictService.giveBack();
    console.log(this.ans);
    this.text = this. ans[0];
    this.classify = this.categories[this.ans[1]];
  }

  add(){
    this.customersService.addArticle(this.userId, this.text, this.classify)
  }
  


  predict(years, income){
    console.log(years,income);
    this.predictService.classify(years,income).subscribe(
      res => {
        console.log(res);
     
      return this.prediction;
     }
      )

      }
  showPrediction(){
    return this.prediction;
  }

  editstate = [];
  constructor(private customersService:CustomersService, public authService:AuthService,private predictService:PredictService ) { }


  deleteCustomer(id:string){
    this.customersService.deleteCustomer(this.userId, id);
    console.log(id);
  } 
  updateCustomer(customer:Customer){
    this.customersService.updateCustomer(this.userId, customer.id, customer.name, customer.years,customer.income)
  }
  
  // add(customer:Customer){
  //   this.customersService.addCustomer(this.userId, customer.name, customer.years,customer.income);
  // }

  addPrediction(userId,id,prediction){
    console.log(id);
    console.log(userId);
    console.log(prediction);


    this.customersService.addPredict(userId,id,prediction)
  }
  ngOnInit(): void {
    
    

    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.customers$ = this.customersService.getCustomers(this.userId);
        
    })
    
  }

}








  

  
