import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';


import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})

export class CustomerFormComponent implements OnInit {

@Input() name:string;
@Input() years:number;
@Input() id:string;
@Input() income:number;
@Input() formType:string;
@Output() update = new EventEmitter<Customer>()
@Output() closeEdit = new EventEmitter<null>()

hasNotError:boolean=true;

updateParent(){
  let customer:Customer = {id:this.id, name:this.name, years:this.years,income:this.income}
  this.update.emit(customer);
  if(this.formType=="Add customer"){
    this.name = null;
    this.years=null;
    this.income=null;

  };
 
}


tellparentToClose(){
  this.closeEdit.emit();
  
}


validateAge(year:number){
  if(year>24 || year<0){
    this.hasNotError=false;
    console.log(this.hasNotError);
  };
}

onSubmit(){}


  constructor() { }

  ngOnInit(): void {
  }

}
